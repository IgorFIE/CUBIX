package com.cubix.screens.level;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.*;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g3d.*;
import com.cubix.screens.AbstractScreen;
import com.cubix.screens.ScreenEnum;
import com.cubix.utilities.*;

/**
 * Created by pacifist on 08-08-2016.
 */
public class LevelScreen extends AbstractScreen {

    private PerspectiveCamera camera;
    private ModelBatch modelBatch;
    private ModelInstance boardInstance;
    private Environment environment;

    private SpriteBatch spriteBatch;
    private BitmapFont font;

    private GameLogic gameLogic;

    public LevelScreen() {
        super();
    }

    @Override
    public void buildStage() {
        environment =  CommonObjects.getInstance().getEnvironment();
        camera = CommonObjects.getInstance().getCamera();
        modelBatch = new ModelBatch();

        boardInstance = CommonObjects.getInstance().getBoardInstance();
        spriteBatch = new SpriteBatch();

        gameLogic = new GameLogic(modelBatch,environment);

        addListener(gameLogic.getPlayer().getPlayerControls());

        font = new BitmapFont();
        font.setColor(Color.RED);
        font.getData().setScale(3, 3);
    }

    @Override
    public void render(float delta) {
        super.render(delta);
        lidGDXSetUp();
        //debugFPS(delta);

        if (!gameLogic.isGameOver()) {
            gameLogic.updateGameLogic();
            gameLogic.checkCollisionWithBall();
        }
        gameLogic.drawCubes();
        ifGameOverDisplayScreen();

        gameLogic.drawBall();
        gameLogic.updateGameScore();
        modelBatch.end();
    }

    private void ifGameOverDisplayScreen() {
        if(gameLogic.isGameOver()){
            ScreenManager.getInstance().showScreen(ScreenEnum.GAME_OVER_SCREEN, this);
        }
    }

    private void lidGDXSetUp() {
        modelBatch.begin(camera);
        modelBatch.render(boardInstance,environment);
    }

    private void debugFPS(float delta) {
        spriteBatch.begin();
        if(((int) (1 / delta)) < 55){
            font.draw(spriteBatch, "FPS: " + ((int) (1 / delta)), Gdx.graphics.getWidth() / 8, 40);
        }
        spriteBatch.end();
    }

    @Override
    public void renderPersistedObjects() {
        gameLogic.drawCubes();
        gameLogic.drawBall();
        gameLogic.updateGameScore();
    }

    public ModelBatch getModelBatch() {
        return modelBatch;
    }

    public SpriteBatch getSpriteBatch() {
        return spriteBatch;
    }

    @Override
    public Camera getCamera() {
        return super.getCamera();
    }

    @Override
    public void dispose() {}
}
