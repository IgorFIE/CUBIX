package com.cubix.screens.level;

import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.utils.Array;
import com.cubix.gameobjects.GameObjectLine;
import com.cubix.gameobjects.board.CubeField;
import com.cubix.gameobjects.boardobjects.Cube;
import com.cubix.player.Player;
import com.cubix.utilities.CommonObjects;
import com.cubix.utilities.GameScore;
import com.cubix.utilities.GameSounds;

import java.util.Iterator;

/**
 * Created by pacifist on 30-07-2017.
 */
public class GameLogic {

    private Player player;
    private CubeField cubeField;
    private GameScore scoreBoard;

    private Array<GameObjectLine> gameObjectLineList;
    private boolean isGameOver;

    private int loopsUntilLinesMovements;
    private int loopsUntilNewLine;
    private int numberOfHolesPerLine;
    private int difficultyCounter;
    private int lineDelay;
    private int trigger;

    private int gameScore;
    private boolean wasGameScoreChanged;
    private ModelBatch modelBatch;
    private Environment environment;

    public GameLogic(ModelBatch modelBatch, Environment environment) {
        this.modelBatch = modelBatch;
        this.environment = environment;
        initGameProperties();
    }

    private void initGameProperties() {
        player = new Player();
        gameObjectLineList = new Array<GameObjectLine>();
        cubeField = CommonObjects.getInstance().getCubeField();
        scoreBoard = CommonObjects.getInstance().getGameScore();

        loopsUntilLinesMovements = 0;
        gameScore = 0;
        wasGameScoreChanged = true;
        lineDelay = 35;
        numberOfHolesPerLine = 1;
        trigger = 15;
    }

    public void updateGameLogic() {
        if (14 == loopsUntilLinesMovements) {
            moveLines();
            createLine();
            increaseDifficulty();
            loopsUntilNewLine++;
            loopsUntilLinesMovements = 0;
        }
        loopsUntilLinesMovements++;
    }

    private void moveLines() {
        Iterator<GameObjectLine> it = gameObjectLineList.iterator();
        GameObjectLine gameObjectLine;

        while (it.hasNext()) {
            gameObjectLine = it.next();
            clearFieldLine(gameObjectLine);

            if (gameObjectLine.getLineRow() >= 14) {
                GameSounds.getInstance().playLineDeathSound();
                it.remove();
                wasGameScoreChanged = true;
                gameScore++;
            } else {
                gameObjectLine.incrementLineRow();
                setFieldLine(gameObjectLine);
            }
        }
    }

    private void clearFieldLine(GameObjectLine gameObjectLine) {
        for(Cube cube : cubeField.getCubeRow(gameObjectLine.getLineRow())){
            cube.setVisibility(false);
        }
    }

    private void setFieldLine(GameObjectLine gameObjectLine) {
        Cube[] cube = cubeField.getCubeRow(gameObjectLine.getLineRow());
        for (int position = 0; position < cube.length; position++) {
            cube[position].setVisibility(gameObjectLine.getGameObjectLine()[position]);
        }
    }

    private void createLine() {
        if (loopsUntilNewLine > trigger) {
            GameSounds.getInstance().playLineCreationSound();
            GameObjectLine gameObjectLine = new GameObjectLine(9, numberOfHolesPerLine);
            setFieldLine(gameObjectLine);
            gameObjectLineList.add(gameObjectLine);
            loopsUntilNewLine = 0;
        }
    }

    private void increaseDifficulty() {
        difficultyCounter++;
        if (difficultyCounter > (numberOfHolesPerLine * lineDelay)) {
            difficultyCounter = 0;
            if (trigger != 1) {
                trigger = trigger - 2;

                if (trigger == 3 || trigger == 1) {
                    lineDelay *= 2;
                    numberOfHolesPerLine++;
                }

            } else {
                if (numberOfHolesPerLine != 1) {
                    numberOfHolesPerLine--;
                }
            }
        }
    }

    public void checkCollisionWithBall() {
        for(GameObjectLine gameObjectLine : gameObjectLineList){
            if (gameObjectLine.getLineRow() == 14 - player.getPlayerBallRow()) {
                if (gameObjectLine.getGameObjectLine()[player.getPlayerBallColumn()]) {
                    GameSounds.getInstance().playDeathSound();
                    player.deactivatePlayerBallVisibility();
                    isGameOver = true;
                    break;
                }
            }
        }
    }

    public void drawCubes() {
        for(GameObjectLine gameObjectLine : gameObjectLineList){
            for (Cube cube: cubeField.getCubeRow(gameObjectLine.getLineRow())) {
                if (cube.isVisible()) {
                    cube.draw(modelBatch,environment);
                }
            }
        }
    }

    public void drawBall() {
        if (player.getPlayerBall().isVisible()) {
            player.getPlayerBall().draw(modelBatch,environment);
        }
    }

    public void updateGameScore() {
        if(wasGameScoreChanged){
            wasGameScoreChanged = false;
            scoreBoard.setScore(gameScore);
        }
        scoreBoard.drawScore(modelBatch,environment);
    }

    public boolean isGameOver() {
        return isGameOver;
    }

    public Player getPlayer() {
        return player;
    }
}
