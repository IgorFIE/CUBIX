package com.cubix.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.cubix.utilities.*;
import com.cubix.utilities.GameScore;

/**
 * Created by pacifist on 08-08-2016.
 */
public class MenuScreen extends AbstractScreen {

    private PerspectiveCamera camera;
    private ModelBatch modelBatch;
    private SpriteBatch spriteBatch;
    private Texture menuImg;
    private ModelInstance background;

    private Environment environment;
    private GameScore gameScoreBoard;

    private AbstractScreen lastGameScreen;

    public MenuScreen(AbstractScreen screen) {
        super();
        lastGameScreen = screen;
    }

    @Override
    public void buildStage() {
        environment = CommonObjects.getInstance().getEnvironment();
        camera = CommonObjects.getInstance().getCamera();
        retrieveModelBatch();
        spriteBatch = new SpriteBatch();

        background = CommonObjects.getInstance().getBoardInstance();
        menuImg = GameAssets.getInstance().getAssetManager().get(GameAssets.MENU);
        gameScoreBoard = new GameScore();

        createListenersForMenu();
    }

    @Override
    public void render(float delta) {
        super.render(delta);

        modelBatch.begin(camera);
        retrieveLastGameObjects();
        modelBatch.render(background, environment);
        modelBatch.end();

        spriteBatch.begin();
        spriteBatch.draw(menuImg,0,0,Gdx.graphics.getWidth(),Gdx.graphics.getHeight());
        spriteBatch.end();
    }

    private void retrieveLastGameObjects() {
        if(lastGameScreen.getModelBatch() != null){
            lastGameScreen.renderPersistedObjects();
        } else {
            gameScoreBoard.setScore(0);
            gameScoreBoard.drawScore(modelBatch, environment);
        }
    }

    private void retrieveModelBatch() {
        if(lastGameScreen.getModelBatch() == null){
            modelBatch = new ModelBatch();
        } else {
            modelBatch = lastGameScreen.getModelBatch();
        }
    }

    private void createListenersForMenu() {
        addListener(new InputListener(){
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                startButtonListener(x, y);
                return false;
            }
        });
    }

    private void startButtonListener(float x, float y) {
        if (x > Gdx.graphics.getWidth()/6 && x < Gdx.graphics.getWidth() - (Gdx.graphics.getWidth()/6)
                && y > Gdx.graphics.getHeight()/4 && y < Gdx.graphics.getHeight() - (Gdx.graphics.getHeight()/4)) {
            CommonObjects.getInstance().getListenerDelay();
            ScreenManager.getInstance().showScreen(ScreenEnum.LEVEL_SCREEN);
            GameSounds.getInstance().playTouchSound();
        }
    }

    @Override
    public void dispose() {
        modelBatch.dispose();
        spriteBatch.dispose();
        lastGameScreen.getActors().clear();
        if(lastGameScreen.getSpriteBatch() != null){
            lastGameScreen.getSpriteBatch().dispose();
        }
        getActors().clear();
    }
}
