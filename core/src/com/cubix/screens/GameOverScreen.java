package com.cubix.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.cubix.utilities.*;

/**
 * Created by pacifist on 10-08-2016.
 */
public class GameOverScreen extends AbstractScreen {

    private Texture gameOverTexture;
    private AbstractScreen lastGameScreen;

    public GameOverScreen(AbstractScreen screen) {
        super();
        lastGameScreen = screen;
    }

    @Override
    public void buildStage() {
        gameOverTexture = GameAssets.getInstance().getAssetManager().get(GameAssets.GAMEOVER);
        createListener();
    }

    private void createListener() {
        addListener(new InputListener(){
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
            if (x > Gdx.graphics.getWidth()/6 && x < Gdx.graphics.getWidth() - (Gdx.graphics.getWidth()/6)
                    && y > Gdx.graphics.getHeight()/4 && y < Gdx.graphics.getHeight() - (Gdx.graphics.getHeight()/4)) {
                CommonObjects.getInstance().getListenerDelay();
                CommonObjects.getInstance().getListenerDelay();
                ScreenManager.getInstance().showScreen(ScreenEnum.MENU_SCREEN, lastGameScreen);
                GameSounds.getInstance().playTouchSound();
            }
            return false;
            }
        });
    }

    @Override
    public void render(float delta) {
        super.render(delta);
        lastGameScreen.render(delta);
        lastGameScreen.getSpriteBatch().begin();
        lastGameScreen.getSpriteBatch().draw(gameOverTexture,0,0,Gdx.graphics.getWidth(),Gdx.graphics.getHeight());
        lastGameScreen.getSpriteBatch().end();
    }

    @Override
    public void dispose() {
        getActors().clear();
    }
}
