package com.cubix.screens;

import com.cubix.screens.level.LevelScreen;

/**
 * Created by pacifist on 08-08-2016.
 */
public enum ScreenEnum {

    MENU_SCREEN{
        public AbstractScreen getScreen(Object... params) {
            return new MenuScreen((AbstractScreen) params[0]);
        }
    },

    LEVEL_SCREEN {
        public AbstractScreen getScreen(Object... params) {
            return new LevelScreen();
        }
    },

    GAME_OVER_SCREEN {
        public AbstractScreen getScreen(Object... params) {
            return new GameOverScreen((AbstractScreen) params[0]);
        }
    };

    public abstract AbstractScreen getScreen(Object... params);
}
