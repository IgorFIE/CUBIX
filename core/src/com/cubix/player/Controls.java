package com.cubix.player;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.cubix.utilities.GameSounds;

/**
 * Created by pacifist on 08-08-2016.
 */
public class Controls extends InputListener {

    private Player player;

    public Controls(Player player) {
        this.player = player;
    }

    @Override
    public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
        moveBallRight(x);
        moveBallLeft(x);
        GameSounds.getInstance().playTouchSound();
        return false;
    }

    @Override
    public boolean keyDown(InputEvent event, int keycode) {
        if(keycode == Input.Keys.LEFT) player.movePlayerBall(-1);
        if(keycode == Input.Keys.RIGHT) player.movePlayerBall(1);
        GameSounds.getInstance().playTouchSound();
        return false;
    }

    private void moveBallLeft(float x) {
        if (x > Gdx.graphics.getWidth()/2) {
            player.movePlayerBall(1);
        }
    }

    private void moveBallRight(float x) {
        if (x < Gdx.graphics.getWidth()/2) {
            player.movePlayerBall(-1);
        }
    }
}
