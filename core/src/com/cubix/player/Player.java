package com.cubix.player;

import com.cubix.gameobjects.board.BallField;
import com.cubix.utilities.CommonObjects;
import com.cubix.utilities.ObjectPosition;
import com.cubix.gameobjects.boardobjects.Ball;

/**
 * Created by codecadet on 02/06/16.
 */
public class Player {

    private ObjectPosition playerBallPosition;
    private Controls playerControls;
    private BallField ballField;

    public Player() {
        initPlayerControls();
        initPlayerPosition();
        initPlayerBall();
    }

    private void initPlayerControls() {
        playerControls = new Controls(this);
    }

    private void initPlayerBall() {
        ballField = CommonObjects.getInstance().getBallField();
        ballField.setBallVisibility(0,4,true);
    }

    private void initPlayerPosition() {
        playerBallPosition = new ObjectPosition();
        playerBallPosition.setRow(0);
        playerBallPosition.setColumn(4);
    }

    public void movePlayerBall(int column) {
        if (getPlayerBallColumn() + column >= 0 && getPlayerBallColumn() + column <= 8) {
            updatePlayerBall(column);
        }
    }

    private void updatePlayerBall(int column) {
        ballField.setBallVisibility(getPlayerBallRow(),getPlayerBallColumn(),false);
        playerBallPosition.setColumn(getPlayerBallColumn() + column);
        ballField.setBallVisibility(getPlayerBallRow(),getPlayerBallColumn(),true);
    }

    public void deactivatePlayerBallVisibility() {
        ballField.setBallVisibility(getPlayerBallRow(),getPlayerBallColumn(),false);
    }

    public Ball getPlayerBall() {
        return ballField.getSpecificBall(playerBallPosition.getRow(), playerBallPosition.getColumn());
    }

    public int getPlayerBallColumn() {
        return playerBallPosition.getColumn();
    }

    public int getPlayerBallRow() {
        return playerBallPosition.getRow();
    }

    public Controls getPlayerControls() {
        return playerControls;
    }
}
