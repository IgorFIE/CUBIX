package com.cubix;

import com.badlogic.gdx.Game;
import com.cubix.screens.ScreenEnum;
import com.cubix.utilities.GameAssets;
import com.cubix.utilities.ScreenManager;

public class Main extends Game {
	
	@Override
	public void create () {
		loadGameAssets();
		initializeMenuScreen();
	}

	private void initializeMenuScreen() {
		ScreenManager.getInstance().initialize(this);
		ScreenManager.getInstance().showScreen(ScreenEnum.MENU_SCREEN,ScreenEnum.LEVEL_SCREEN.getScreen());
	}

	private void loadGameAssets() {
		GameAssets.getInstance().loadGameAssets();
		GameAssets.getInstance().getAssetManager().finishLoading();
	}
}
