package com.cubix.gameobjects.board;

import com.cubix.gameobjects.GameObjectsFabric;
import com.cubix.gameobjects.boardobjects.BoardObjectType;
import com.cubix.gameobjects.boardobjects.Cube;

/**
 * Created by codecadet on 24/05/16.
 */
public class CubeField {

    private Cube[][] cubeField = new Cube[15][9];

    public CubeField(){
        createCubeField();
    }

    private void createCubeField(){
        GameObjectsFabric.createField(BoardObjectType.CUBE,cubeField);
    }

    public Cube[] getCubeRow(int row){
        return cubeField[row];
    }
}
