package com.cubix.gameobjects.board;

import com.cubix.gameobjects.GameObjectsFabric;
import com.cubix.gameobjects.boardobjects.Ball;
import com.cubix.gameobjects.boardobjects.BoardObjectType;

/**
 * Created by pacifist on 24-06-2017.
 */
public class BallField {

    private Ball[][] ballField = new Ball[1][9];

    public BallField() {
        createBallField();
    }

    private void createBallField() {
        GameObjectsFabric.createField(BoardObjectType.BALL,ballField);
    }

    public void setBallVisibility(int ballRow, int ballColumn, boolean visibility) {
        ballField[ballRow][ballColumn].setVisibility(visibility);
    }

    public Ball getSpecificBall(int ballRow, int ballColumn) {
        return ballField[ballRow][ballColumn];
    }
}
