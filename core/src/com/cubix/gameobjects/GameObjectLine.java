package com.cubix.gameobjects;

import com.cubix.utilities.ObjectPosition;

import java.util.Arrays;

/**
 * Created by codecadet on 01/06/16.
 */
public class GameObjectLine {

    private boolean[] gameObjectLine;
    private ObjectPosition linePosition;

    public GameObjectLine(int lineSize, int holesInLine) {
        createLine(lineSize, holesInLine);
    }

    private void createLine(int lineSize, int holesInLine) {
        gameObjectLine = new boolean[lineSize];
        Arrays.fill(gameObjectLine, true);
        linePosition = new ObjectPosition();
        openSpacesInLine(holesInLine);
    }

    private void openSpacesInLine(int holesInLine) {
        int counter = 0;
        int holePosition;

        while (counter < holesInLine) {
            holePosition = (int) (Math.random() * gameObjectLine.length);

            if (holePosition == 0) {
                counter = checkEdge(counter, 1, holePosition);
            } else if (holePosition == 8) {
                counter = checkEdge(counter, -1, holePosition);
            } else {
                gameObjectLine[holePosition] = false;
                counter++;
            }
        }
    }

    private int checkEdge(int counter, int edgeBalance, int holePosition) {
        if (gameObjectLine[holePosition] && gameObjectLine[holePosition + edgeBalance]) {
            gameObjectLine[holePosition] = false;
            counter++;
        }
        return counter;
    }

    public int getLineRow() {
        return linePosition.getRow();
    }

    public void incrementLineRow() {
        linePosition.setRow(linePosition.getRow() + 1);
    }

    public boolean[] getGameObjectLine() {
        return gameObjectLine;
    }
}
