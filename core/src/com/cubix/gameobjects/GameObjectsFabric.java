package com.cubix.gameobjects;

import com.cubix.gameobjects.boardobjects.*;

/**
 * Created by pacifist on 24-06-2017.
 */
public class GameObjectsFabric {

    public static BoardObject createBoardObject(BoardObjectType boardObjectType,int boardObjectRow,int boardObjectColumn){
        switch (boardObjectType){
            case CUBE:
                return new Cube(boardObjectRow,boardObjectColumn);
            case BALL:
                return new Ball(boardObjectRow,boardObjectColumn);
            default:
                return null;
        }
    }

    public static void createField(BoardObjectType boardObjectType, BoardObject[][] boardObjectsField) {
        for (int row = 0; row < boardObjectsField.length; row++) {
            for (int column = 0; column < boardObjectsField[row].length; column++) {
                boardObjectsField[row][column] = createBoardObject(boardObjectType,row,column);
            }
        }
    }
}
