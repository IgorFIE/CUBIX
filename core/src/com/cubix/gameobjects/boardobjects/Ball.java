package com.cubix.gameobjects.boardobjects;

import com.badlogic.gdx.graphics.g3d.*;
import com.badlogic.gdx.graphics.g3d.attributes.TextureAttribute;
import com.badlogic.gdx.math.Vector3;
import com.cubix.utilities.GameAssets;

/**
 * Created by codecadet on 23/05/16.
 */
public class Ball implements BoardObject{

    private boolean isVisible;
    private ModelInstance ballInstance;

    public Ball(int ballRow, int ballColumn){
        configureBallInstance(ballRow, ballColumn);
    }

    private void configureBallInstance(int ballRow, int ballColumn) {
        ballInstance = new ModelInstance(GameAssets.getInstance().getAssetManager().get(GameAssets.BALL));
        TextureAttribute ballTexture = new TextureAttribute(TextureAttribute.Diffuse,
                GameAssets.getInstance().getAssetManager().get(GameAssets.BAllSPRITE));

        ballInstance.materials.get(0).set(ballTexture);
        ballInstance.transform.translate(ballColumn*6,ballRow,0);
        ballInstance.transform.rotate(Vector3.X,45);
        ballInstance.transform.rotate(Vector3.Z,-45 *ballColumn);
    }

    public void draw(ModelBatch batch, Environment environment){
        batch.render(ballInstance,environment);
    }

    public void setVisibility(boolean visible) {
        isVisible = visible;
    }

    public boolean isVisible() {
        return isVisible;
    }
}
