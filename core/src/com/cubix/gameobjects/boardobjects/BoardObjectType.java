package com.cubix.gameobjects.boardobjects;

/**
 * Created by pacifist on 24-06-2017.
 */
public enum BoardObjectType {
    BALL,
    CUBE
}
