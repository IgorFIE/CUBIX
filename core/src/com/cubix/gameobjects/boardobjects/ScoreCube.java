package com.cubix.gameobjects.boardobjects;

import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.cubix.utilities.GameAssets;

/**
 * Created by pacifist on 08-01-2017.
 */
public class ScoreCube implements BoardObject{

    private boolean isVisible;
    private ModelInstance scoreCubeInstance;

    public ScoreCube(int place,int row, int col) {
        configureScoreCubeInstance(place, row, col);
    }

    private void configureScoreCubeInstance(int place, int row, int col) {
        scoreCubeInstance = new ModelInstance(GameAssets.getInstance().getAssetManager().get(GameAssets.CUBE));
        scoreCubeInstance.transform.scale(0.1f,0.1f,0.1f);
        scoreCubeInstance.transform.translate(((col - (place * 4) + 36.5f)* 7), ((14 - row) - 22) * 7, 0);
    }

    public void draw(ModelBatch batch, Environment environment) {
        batch.render(scoreCubeInstance, environment);
    }

    public void setVisibility(boolean visible) {
        isVisible = visible;
    }

    public boolean isVisible() {
        return isVisible;
    }
}
