package com.cubix.gameobjects.boardobjects;

import com.badlogic.gdx.graphics.g3d.*;
import com.cubix.utilities.GameAssets;

/**
 * Created by codecadet on 23/05/16.
 */
public class Cube implements BoardObject{

    private boolean isVisible;
    private ModelInstance cubeInstance;

    public Cube(int row, int col) {
        configureCubeInstance(row, col);
    }

    private void configureCubeInstance(int row, int col) {
        cubeInstance = new ModelInstance(GameAssets.getInstance().getAssetManager().get(GameAssets.CUBE));
        cubeInstance.transform.translate(col * 6, (14 - row) * 6, 0);
    }

    public void draw(ModelBatch batch, Environment environment) {
        batch.render(cubeInstance, environment);
    }

    public void setVisibility(boolean visible) {
        isVisible = visible;
    }

    public boolean isVisible() {
        return isVisible;
    }
}
