package com.cubix.gameobjects.boardobjects;

import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.ModelBatch;

/**
 * Created by pacifist on 24-06-2017.
 */
public interface BoardObject {

    void draw(ModelBatch batch, Environment environment);

    void setVisibility(boolean visible);

    boolean isVisible();
}
