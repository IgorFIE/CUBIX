package com.cubix.utilities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g3d.Model;

/**
 * Created by pacifist on 11-08-2016.
 */
public class GameAssets {

    private static GameAssets gameAssets;
    private AssetManager assetManager = new AssetManager();

    public static final AssetDescriptor<Sound> TOUCH_SOUND = new AssetDescriptor<Sound>("art/sounds/touch_sound.mp3", Sound.class);
    public static final AssetDescriptor<Sound> DEATH_SOUND = new AssetDescriptor<Sound>("art/sounds/death_sound.mp3", Sound.class);
    public static final AssetDescriptor<Sound> LINE_CREATION_SOUND = new AssetDescriptor<Sound>("art/sounds/line_creation_sound.mp3", Sound.class);
    public static final AssetDescriptor<Sound> LINE_DEATH_SOUND = new AssetDescriptor<Sound>("art/sounds/line_death_sound.mp3", Sound.class);

    public static final AssetDescriptor<Texture> MENU = new AssetDescriptor<Texture>("art/Menu.png", Texture.class);
    public static final AssetDescriptor<Texture> GAMEOVER = new AssetDescriptor<Texture>("art/GameOver.png", Texture.class);

    public static final AssetDescriptor<Model> BOARD = new AssetDescriptor<Model>("art/models/board.obj",Model.class);
    public static final AssetDescriptor<Texture> BOARDSPRITE = new AssetDescriptor<Texture>("art/models/board.png",Texture.class);

    public static final AssetDescriptor<Model> BALL = new AssetDescriptor<Model>("art/models/ball.obj",Model.class);
    public static final AssetDescriptor<Texture> BAllSPRITE = new AssetDescriptor<Texture>("art/models/ball.png",Texture.class);

    public static final AssetDescriptor<Model> CUBE = new AssetDescriptor<Model>("art/models/cube.obj",Model.class);
    public static final AssetDescriptor<Texture> CUBESPRITE = new AssetDescriptor<Texture>("art/models/cube.png",Texture.class);

    private GameAssets() {
    }

    public static GameAssets getInstance(){
        if(gameAssets == null){
            gameAssets = new GameAssets();
        }
        return gameAssets;
    }

    public void loadGameAssets(){
        loadAudio();
        loadUIAssets();
        loadBoardAssets();
        loadCubeAssets();
        loadBallAssets();
    }

    private void loadAudio(){
        assetManager.load(LINE_CREATION_SOUND);
        assetManager.load(LINE_DEATH_SOUND);
        assetManager.load(TOUCH_SOUND);
        assetManager.load(DEATH_SOUND);
    }

    private void loadUIAssets() {
        assetManager.load(MENU);
        assetManager.load(GAMEOVER);
    }

    private void loadBallAssets() {
        assetManager.load(BALL);
        assetManager.load(BAllSPRITE);
    }

    private void loadCubeAssets() {
        assetManager.load(CUBE);
        assetManager.load(CUBESPRITE);
    }

    private void loadBoardAssets() {
        assetManager.load(BOARD);
        assetManager.load(BOARDSPRITE);
    }

    public AssetManager getAssetManager() {
        return assetManager;
    }
}
