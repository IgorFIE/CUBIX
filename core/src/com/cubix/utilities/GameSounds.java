package com.cubix.utilities;

import com.badlogic.gdx.audio.Sound;

/**
 * Created by pacifist on 29-07-2017.
 */
public class GameSounds {

    private static GameSounds gameSounds;

    private static Sound touchSound;
    private static Sound deathSound;
    private static Sound lineCreationSound;
    private static Sound lineDeathSound;

    private GameSounds() {}

    public static GameSounds getInstance(){
        if(gameSounds == null){
            gameSounds = new GameSounds();
            initSoundsValues();
        }
        return gameSounds;
    }

    private static void initSoundsValues(){
        touchSound = GameAssets.getInstance().getAssetManager().get(GameAssets.TOUCH_SOUND);
        deathSound = GameAssets.getInstance().getAssetManager().get(GameAssets.DEATH_SOUND);
        lineCreationSound = GameAssets.getInstance().getAssetManager().get(GameAssets.LINE_CREATION_SOUND);
        lineDeathSound = GameAssets.getInstance().getAssetManager().get(GameAssets.LINE_DEATH_SOUND);
    }

    public static void playTouchSound() {
        touchSound.play();
    }

    public static void playDeathSound() {
        deathSound.setVolume(deathSound.play(),1f);
    }

    public static void playLineCreationSound() {
        lineCreationSound.setVolume(lineCreationSound.play(),0.5f);
    }

    public static void playLineDeathSound() {
        lineDeathSound.setVolume(lineDeathSound.play(),0.5f);
    }
}
