package com.cubix.utilities;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Screen;
import com.cubix.screens.AbstractScreen;
import com.cubix.screens.ScreenEnum;

/**
 * Created by pacifist on 08-08-2016.
 */
public class ScreenManager {

    private static ScreenManager screen;
    private Game game;

    private ScreenManager() {
    }

    public static ScreenManager getInstance(){
        if(screen == null){
            screen = new ScreenManager();
        }

        return screen;
    }

    public void initialize(Game game){
        this.game = game;
    }

    public void showScreen(ScreenEnum screenEnum, Object... params) {
        disposePreviousScreen();
        configureNewScreen(screenEnum, params);
    }

    private void disposePreviousScreen() {
        Screen previousScreen = game.getScreen();
        if (previousScreen != null) {
            previousScreen.dispose();
        }
        System.gc();
    }

    private void configureNewScreen(ScreenEnum screenEnum, Object[] params) {
        AbstractScreen newScreen = screenEnum.getScreen(params);
        newScreen.buildStage();
        game.setScreen(newScreen);
    }
}
