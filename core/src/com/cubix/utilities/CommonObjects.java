package com.cubix.utilities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.environment.DirectionalLight;
import com.badlogic.gdx.utils.TimeUtils;
import com.cubix.gameobjects.board.BallField;
import com.cubix.gameobjects.board.CubeField;

/**
 * Created by pacifist on 26-06-2017.
 */
public class CommonObjects {

    private static CommonObjects commonObjects;

    private static Environment environment;
    private static PerspectiveCamera camera;
    private static ModelInstance boardInstance;
    private static CubeField cubeField;
    private static BallField ballField;
    private static GameScore gameScore;

    private CommonObjects() {
    }

    public static CommonObjects getInstance(){
        if(commonObjects == null){
            commonObjects = new CommonObjects();
            environment = createEnvironment();
            camera = createCamera();
            boardInstance = createBoard();
            cubeField = new CubeField();
            ballField = new BallField();
            gameScore = new GameScore();
        }
        return commonObjects;
    }

    private static Environment createEnvironment(){
        Environment environment = new Environment();
        environment.set(new ColorAttribute(ColorAttribute.AmbientLight, 0.4f, 0.4f, 0.4f, 1f));
        environment.add(new DirectionalLight().set(0.8f, 0.8f, 0.8f, 0f, 0f, -100f));
        return environment;
    }

    private static PerspectiveCamera createCamera(){
        PerspectiveCamera camera = new PerspectiveCamera(50, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        camera.position.set(24f, 42f, 110f);
        camera.lookAt(24,42,0);
        camera.near = 1f;
        camera.far = 300f;
        camera.update();
        return camera;
    }

    private static ModelInstance createBoard(){
        ModelInstance boardInstance = new ModelInstance(GameAssets.getInstance().getAssetManager().get(GameAssets.BOARD));
        boardInstance.materials.get(0).set(ColorAttribute.createDiffuse(new Color(0.17f,0.17f,0.22f,0f)));
        boardInstance.transform.translate(27f,42,0);
        boardInstance.transform.scale(1.5f,1.5f,1.5f);
        return boardInstance;
    }

    public GameScore getGameScore() {
        return gameScore;
    }

    public CubeField getCubeField() {
        return cubeField;
    }

    public BallField getBallField() {
        return ballField;
    }

    public Environment getEnvironment() {
        return environment;
    }

    public PerspectiveCamera getCamera() {
        return camera;
    }

    public ModelInstance getBoardInstance() {
        return boardInstance;
    }

    public void getListenerDelay() {
        long time = System.currentTimeMillis();
        while (System.currentTimeMillis() < time + 500){}
    }
}
