package com.cubix.utilities;

import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.utils.Array;
import com.cubix.gameobjects.boardobjects.BoardObject;
import com.cubix.gameobjects.boardobjects.ScoreCube;

/**
 * Created by pacifist on 08-01-2017.
 */
public class GameScore {

    private BoardObject[][][] scoreBoard = new BoardObject[3][5][5];
    private Array<BoardObject> scoreCubesVisible = new Array<BoardObject>();

    public GameScore() {
        createScoreBoardCubes();
    }

    private void createScoreBoardCubes() {
        for (int position = 0; position < scoreBoard.length; position++) {
            for (int y = 0; y < scoreBoard[position].length; y++) {
                for (int x = 0; x < scoreBoard[position][y].length; x++) {
                    scoreBoard[position][y][x] = new ScoreCube(position, y, x);
                }
            }
        }
    }

    public void setScore(int score) {
        scoreCubesVisible.clear();
        int[] numbers = convertScoreToArray(score);
        for(int position = 0; position < numbers.length; position++){
            switch (position){
                case 0:
                    setVisibility(2, getNumberDictionary(numbers[position]));
                    break;
                case 1:
                    setVisibility(1, getNumberDictionary(numbers[position]));
                    break;
                case 2:
                    setVisibility(0, getNumberDictionary(numbers[position]));
                    break;
            }
        }
    }

    public void drawScore(ModelBatch batch, Environment environment ){
        for (BoardObject scoreCube : scoreCubesVisible) {
            scoreCube.draw(batch, environment);
        }
    }

    private boolean[][] getNumberDictionary(int number){
        if(number == 0) return NumberDictionary.NUMBER_0;
        if(number == 1) return NumberDictionary.NUMBER_1;
        if(number == 2) return NumberDictionary.NUMBER_2;
        if(number == 3) return NumberDictionary.NUMBER_3;
        if(number == 4) return NumberDictionary.NUMBER_4;
        if(number == 5) return NumberDictionary.NUMBER_5;
        if(number == 6) return NumberDictionary.NUMBER_6;
        if(number == 7) return NumberDictionary.NUMBER_7;
        if(number == 8) return NumberDictionary.NUMBER_8;
        if(number == 9) return NumberDictionary.NUMBER_9;
        return NumberDictionary.NUMBER_0;
    }

    private void setVisibility(int position, boolean[][] visibility){
        for (int y = 0; y < scoreBoard[position].length; y++) {
            for (int x = 0; x < scoreBoard[position][y].length; x++) {
                scoreBoard[position][y][x].setVisibility(visibility[y][x]);
                if(visibility[y][x]) {
                    scoreCubesVisible.add(scoreBoard[position][y][x]);
                }
            }
        }
    }

    private int[] convertScoreToArray(int score){
        String scoreInString = String.valueOf(score);
        int[] scoreConverted = new int[3];
        for(int position = 0; position < scoreInString.length();position++){
            scoreConverted[((scoreConverted.length - position)-1)] = scoreInString.charAt((scoreInString.length()-1)-position)-'0';
        }
        return scoreConverted;
    }

}
