package com.cubix.utilities;

/**
 * Created by pacifist on 08-01-2017.
 */
public class NumberDictionary {
    public static final boolean[][] NUMBER_0 =  {{false,true,true,true,false},
                                                {false,true,false,true,false},
                                                {false,true,false,true,false},
                                                {false,true,false,true,false},
                                                {false,true,true,true,false}};

    public static final boolean[][] NUMBER_1 =  {{false,false,true,false,false},
                                                {false,true,true,false,false},
                                                {false,false,true,false,false},
                                                {false,false,true,false,false},
                                                {false,true,true,true,false}};

    public static final boolean[][] NUMBER_2 =  {{false,true,true,true,false},
                                                {false,true,false,true,false},
                                                {false,false,false,true,false},
                                                {false,false,true,false,false},
                                                {false,true,true,true,false}};

    public static final boolean[][] NUMBER_3 =  {{false,true,true,true,false},
                                                {false,false,false,true,false},
                                                {false,false,true,true,false},
                                                {false,false,false,true,false},
                                                {false,true,true,true,false}};

    public static final boolean[][] NUMBER_4 =  {{false,false,false,true,false},
                                                {false,false,true,true,false},
                                                {false,true,false,true,false},
                                                {false,true,true,true,false},
                                                {false,false,false,true,false}};

    public static final boolean[][] NUMBER_5 =  {{false,true,true,true,false},
                                                {false,true,false,false,false},
                                                {false,true,true,true,false},
                                                {false,false,false,true,false},
                                                {false,true,true,true,false}};

    public static final boolean[][] NUMBER_6 =  {{false,true,true,true,false},
                                                {false,true,false,false,false},
                                                {false,true,true,true,false},
                                                {false,true,false,true,false},
                                                {false,true,true,true,false}};

    public static final boolean[][] NUMBER_7 =  {{false,true,true,true,false},
                                                {false,false,false,true,false},
                                                {false,false,false,true,false},
                                                {false,false,true,false,false},
                                                {false,false,true,false,false}};

    public static final boolean[][] NUMBER_8 =  {{false,true,true,true,false},
                                                {false,true,false,true,false},
                                                {false,true,true,true,false},
                                                {false,true,false,true,false},
                                                {false,true,true,true,false}};

    public static final boolean[][] NUMBER_9 =  {{false,true,true,true,false},
                                                {false,true,false,true,false},
                                                {false,true,true,true,false},
                                                {false,false,false,true,false},
                                                {false,true,true,true,false}};
}
