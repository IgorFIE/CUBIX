package com.cubix.utilities;

/**
 * Created by pacifist on 08-08-2016.
 */
public class ObjectPosition {

    private int col;
    private int row;

    public int getColumn() {
        return col;
    }

    public void setColumn(int col) {
        this.col = col;
    }

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }
}
